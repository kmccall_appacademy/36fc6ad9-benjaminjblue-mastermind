class Code
  PEGS = {"b" => "blue", "r" => "red", "g" => "green", "y" => "yellow", "o" => "orange", "p" => "purple"}

  attr_reader :pegs

  def initialize(code)
    @pegs = code
  end

  def [](i)
    pegs[i]
  end

  def ==(code)
    @pegs == code.pegs if code.is_a?(Code)
  end

  def exact_matches(code)
    (0...4).reduce(0) {|total, i| total + (@pegs[i] == code[i] ? 1 : 0)}
  end

  def near_matches(code)
    hash = {}
    exact = (0...4).select{|i| @pegs[i] == code.pegs[i]}

    #pull out exact matches
    code1 = [].replace(@pegs).select.with_index{|_,i| !exact.include?(i)}
    code2 = [].replace(code.pegs).select.with_index{|_,i| !exact.include?(i)}
    code1.each do |ch|
      i = code2.index(ch)
      code2.delete_at(i) if i
    end
    code1.length - code2.length
  end

  def self.parse(string)
    is_parsed = string.downcase.each_char.all? {|ch| PEGS[ch] != nil} && string.length == 4
    if is_parsed
      Code.new(string.downcase.chars)
    else
      raise "valid color codes are: r g b y o p R G B Y O P"
    end
  end

  def self.random
    colors = ["b", "r", "g", "y", "o", "p"]
    code = (0...4).map {|_| colors[rand(colors.length)]}.join
    self.parse(code)
  end

end

class Game
  attr_reader :secret_code, :guess_count

  def initialize(code = Code.random)
    @secret_code = code
    @guess_count = 0
  end

  def play
    exact = 0
    while (exact != 4 && @guess_count < 10)
      guess = self.get_guess
      exact = self.display_matches(guess)
    end
    puts "sorry, out of guesses, the code was #{@secret_code.pegs.join}" if exact < 4
  end

  def get_guess
    @guess_count += 1
    puts "Guess a code"
    string = gets.chomp
    Code.parse(string)
  end

  def display_matches(code)
    exact = @secret_code.exact_matches(code)
    puts "exact matches: #{exact}"
    puts "near matches: #{@secret_code.near_matches(code)}"
    puts "You win! The code was '#{@secret_code.pegs.join}'" if exact == 4
    exact
  end
end

game = Game.new
game.play
